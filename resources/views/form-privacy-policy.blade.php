@extends('layouts.app')

@include('partials.topbar')

@section('content')
    <div class="row" id="privacy-policy">
        <h2>Formulaire de création de politique de confidentialité</h2>
        <div class="col-md-12">
            {!! Form::open(['method' => 'POST', 'url' => '/politique-de-confidentialite']) !!}
            <div class="form-container">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <ol>
                    <li>
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('first', 'Dans le cadre du fonctionnement de votre site web, vous pouvez être amenés à collecter les données de vos utilisateurs. Quels types de données souhaitez-vous collecter ?', ['class' => 'control-label']); !!}</strong>
                                <br>
                                <div class="align-input">
                                    {!! Form::checkbox('first[]', 'Données d\'état-civil, d\'identité, d\'identification...'); !!}
                                    {!! Form::label('firsta','Données d\'état-civil, d\'identité, d\'identification...', 'Données d\'état-civil, d\'identité, d\'identification...') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('first[]', 'Données relatives à la vie personnelle (habitudes de vie, situation familiale, hors données sensibles ou dangereuses)', false, ['id' => 'firstb']); !!}
                                    {!! Form::label('firstb', 'Données relatives à la vie personnelle (habitudes de vie, situation familiale, hors données sensibles ou dangereuses)') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('first[]', 'Données relatives à la vie professionnelle (CV, scolarité formation professionnelle, distinctions...)', false, ['id' => 'firstc']); !!}
                                    {!! Form::label('firstc', 'Données relatives à la vie professionnelle (CV, scolarité formation professionnelle, distinctions...)') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('first[]', 'Informations d\'ordre économique et financier (revenus, situation financière, situation fiscale...)', false, ['id' => 'firstd']); !!}
                                    {!! Form::label('firstd', 'Informations d\'ordre économique et financier (revenus, situation financière, situation fiscale...)') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('first[]', 'Données de connexion (adresses IP, journaux d\'événements...)', false, ['id' => 'firste']); !!}
                                    {!! Form::label('firste', 'Données de connexion (adresses IP, journaux d\'événements...)') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('first[]', 'Données de localisation (déplacements, données GPS, GSM...)', false, ['id' => 'firstf']); !!}
                                    {!! Form::label('firstf', 'Données de localisation (déplacements, données GPS, GSM...)') !!}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('second', 'Souhaitez-vous communiquer les données de vos utilisateurs à des tiers ?', ['class' => 'control-label']); !!}</strong><br>
                                <div class="align-input">
                                    {!! Form::radio('second', '1', false, ['id' => 'yes', 'required' => '']);!!}
                                    {!! Form::label('yes', 'Oui'); !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::radio('second', '2', false, ['id' => 'no', 'required' => '']);!!}
                                    {!! Form::label('no', 'Non'); !!}
                                </div>

                            </div>
                        </div>
                    </li>
                    <li class="display displayNone">
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('third', 'Dans quel cas comptez-vous communiquer les données de vos Utilisateurs à des tiers ? (plusieurs réponses possibles)', ['class' => 'control-label']); !!}</strong>
                                <br>
                                <div class="align-input">
                                    {!! Form::checkbox('third[]', '1', false, ['id' => 'thirda']); !!}
                                    {!! Form::label('thirda', 'Vous communiquerez les données à des tiers en fonction des paramètres du compte de l\'utilisateur') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('third[]', '2', false, ['id' => 'thirdb']); !!}
                                    {!! Form::label('thirdb', 'Vous communiquerez les données à des tiers pour sollicitation commerciale, par vous-même et via vos partenaires (pour des produits et services équivalents à ceux déjà commandés par l\'utilisateur)') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('third[]', '3', false, ['id' => 'thirdc']); !!}
                                    {!! Form::label('thirdc', 'Vous communiquerez les données à des tiers pour revente sous forme anonymisée et globalisée') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('third[]', '4', false, ['id' => 'thirdd']); !!}
                                    {!! Form::label('thirdd', 'Vous communiquerez les données à vos partenaires afin d\'améliorer les services') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('third[]', '5', false, ['id' => 'thirde']); !!}
                                    {!! Form::label('thirde', 'Vous communiquerez les données à vos fournisseurs et filiales afin de fournir les services') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('third[]', '6', false, ['id' => 'thirdf']); !!}
                                    {!! Form::label('thirdf', 'Vous communiquerez les données à des tiers pour du marketing direct') !!}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="display displayNone">
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('fourth', 'Vous vous engagez à ce que les tiers réutilisent les données personnelles de vos utilisateurs selon les modalités suivantes :', ['class' => 'control-label']); !!}</strong>
                                <br>
                                <div class="align-input">
                                    {!! Form::radio('fourth', '1', false, ['id' => 'fourtha']); !!}
                                    {!! Form::label('fourtha', 'Les tiers appliquent des conditions de confidentialité identiques aux vôtres') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::radio('fourth', '2', false, ['id' => 'fourthb']); !!}
                                    {!! Form::label('fourthb', 'Les conditions de confidentialité sont fixées contractuellement entre vous et votre utilisateur') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::radio('fourth', '3', false, ['id' => 'fourthc']); !!}
                                    {!! Form::label('fourthc', 'Les conditions de confidentialité sont celles fixées par le tiers') !!}
                                </div>
                            </div>

                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('fifth', 'En cas de fusion / absorption de votre entreprise, souhaitez-vous obtenir le consentement exprès de vos utilisateurs pour transmettre leurs données personnelles, ou uniquement les informer du changement ?', ['class' => 'control-label']); !!}</strong>
                                <br>
                                <div class="align-input">
                                    {!! Form::radio('fifth', '1', false, ['id' => 'fiftha', 'required' => '']); !!}
                                    {!! Form::label('fiftha', 'Vous souhaitez recueillir le consentement de vos utilisateurs préalablement à l\'opération et à la transmission de leurs données personnelles') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::radio('fifth', '2', false, ['id' => 'fifthb', 'required' => '']); !!}
                                    {!! Form::label('fifthb', 'Vous souhaitez simplement informer vos utilisateurs préalablement à l\'opération et à la transmission de leurs données personnelles') !!}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('sixth', 'Sélectionnez les finalités de traitement des données personnelles collectées (plusieurs choix possibles).', ['class' => 'control-label']); !!}Pour plus d'informations sur la question des finalités de traitement : <a href=https://www.cnil.fr/fr/declaration/ns-048-fichiers-clients-prospects-et-vente-en-ligne">https://www.cnil.fr/fr/declaration/ns-048-fichiers-clients-prospects-et-vente-en-ligne</a></strong>
                                <br><br>
                                <div style="flex-wrap: wrap;" class="align-input">
                                    {!! Form::checkbox('sixth[]', '1', false, ['id' => 'sixtha']); !!}
                                    {!! Form::label('sixtha', 'Effectuer les opérations relatives à la gestion des clients concernant ') !!}
                                    <br>
                                    <ul style="width: 100%; margin-bottom: 20px;">
                                        <li>les contrats ; les commandes ; les livraisons ; les factures ; la
                                            comptabilité
                                            et en
                                            particulier la gestion des comptes clients
                                        </li>
                                        <li>un programme de fidélité au sein d'une entité ou plusieurs entités
                                            juridiques
                                            ;
                                        </li>
                                        <li>le suivi de la relation client tel que la réalisation d'enquêtes de
                                            satisfaction, la
                                            gestion des réclamations et du service après-vente
                                        </li>
                                        <li>la sélection de clients pour réaliser des études, sondages et tests produits
                                            (sauf
                                            consentement des personnes concernées recueilli dans les conditions prévues
                                            à
                                            l’article
                                            6, ces opérations ne doivent pas conduire à l'établissement de profils
                                            susceptibles de
                                            faire apparaître des données sensibles - origines raciales ou ethniques,
                                            opinions
                                            philosophiques, politiques, syndicales, religieuses, vie sexuelle ou santé
                                            des
                                            personnes)
                                        </li>
                                    </ul>
                                    <br><br>
                                </div>
                                <div style="flex-wrap: wrap;" class="align-input">
                                    {!! Form::checkbox('sixth[]', '2', false, ['id' => 'sixthb']); !!}
                                    {!! Form::label('sixthb', 'Effectuer des opérations relatives à la prospection ') !!}
                                    <br>
                                    <ul style="width: 100%; margin-bottom: 20px;">
                                        <li>la gestion d'opérations techniques de prospection (ce qui inclut notamment
                                            les
                                            opérations techniques comme la normalisation, l'enrichissement et la
                                            déduplication)
                                        </li>
                                        <li>la sélection de personnes pour réaliser des actions de fidélisation, de
                                            prospection, de
                                            sondage, de test produit et de promotion. Sauf consentement des personnes
                                            concernées
                                            recueilli dans les conditions prévues par ce texte, ces opérations ne
                                            doivent
                                            pas
                                            conduire à l'établissement de profils susceptibles de faire apparaître des
                                            données
                                            sensibles (origines raciales ou ethniques, opinions philosophiques,
                                            politiques,
                                            syndicales, religieuses, vie sexuelle ou santé des personnes)
                                        </li>
                                        <li>la réalisation d'opérations de sollicitations</li>
                                    </ul>
                                    <br><br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('sixth[]', '3', false, ['id' => 'sixthc']); !!}
                                    {!! Form::label('sixthc', 'L\'élaboration de statistiques commerciales') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('sixth[]', '4', false, ['id' => 'sixthd']); !!}
                                    {!! Form::label('sixthd', 'La cession, la location ou l\'échange de ses fichiers de clients et de ses fichiers de prospects') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('sixth[]', '5', false, ['id' => 'sixthe']); !!}
                                    {!! Form::label('sixthe', 'L’actualisation de ses fichiers de prospection par l’organisme en charge de la gestion de la liste d’opposition au démarchage téléphonique, en application des dispositions du code de la consommation') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('sixth[]', '6', false, ['id' => 'sixthf']); !!}
                                    {!! Form::label('sixthf', 'L\'organisation de jeux concours, de loteries ou de toute opération promotionnelle à l\'exclusion des jeux d\'argent et de hasard en ligne soumis à l\'agrément de l\'Autorité de Régulation des Jeux en Ligne') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('sixth[]', '7', false, ['id' => 'sixthg']); !!}
                                    {!! Form::label('sixthg', 'La gestion des demandes de droit d\'accès, de rectification et d\'opposition') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('sixth[]', '8', false, ['id' => 'sixthh']); !!}
                                    {!! Form::label('sixthh', 'La gestion des impayés et du contentieux, à condition qu\'elle ne porte pas sur des infractions et / ou qu\'elle n\'entraîne pas une exclusion de la personne du bénéfice d\'un droit, d\'une prestation ou d\'un contrat') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::checkbox('sixth[]', '9', false, ['id' => 'sixthi']); !!}
                                    {!! Form::label('sixthi', 'La gestion des avis des personnes sur des produits, services ou contenus') !!}
                                    <br>
                                </div>

                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('seventh', 'Dans quelles conditions l\'utilisateur peut-il accéder aux services du site ?', ['class' => 'control-label']); !!}</strong>
                                <br>
                                <div class="align-input">
                                    {!! Form::radio('seventh', '1', false, ['id' => 'seventha', 'required' => '']); !!}
                                    {!! Form::label('seventha', 'La consultation du site est libre (sans inscription ni identification)') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::radio('seventh', '2', false, ['id' => 'seventhb', 'required' => '']); !!}
                                    {!! Form::label('seventhb', 'L\'utilisation du site nécessite une inscription sans identification préalable de l\'utilisateur (possibilité d\'utiliser un pseudonyme)') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::radio('seventh', '3', false, ['id' => 'seventhc', 'required' => '']); !!}
                                    {!! Form::label('seventhc', 'L\'utilisation du site nécessite une inscription et une identification préalable de l\'utilisateur (exemple : nom, prénom, adresse postale, numéro de téléphone...)') !!}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('eighth', 'Les identifiants électroniques (e-mail) recueillis seront-ils utilisés pour proposer des services complémentaires tels que : permettre à d\'autres personnes de trouver l\'utilisateur, offrir des propositions de mise en relation et / ou commerciales ?', ['class' => 'control-label']); !!}</strong>
                                <br>
                                <div class="align-input">
                                    {!! Form::radio('eighth', '1', false, ['id' => 'eightha', 'required' => '']); !!}
                                    {!! Form::label('eightha', 'Oui') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::radio('eighth', '2', false, ['id' => 'eighthb', 'required' => '']); !!}
                                    {!! Form::label('eighthb', 'Non') !!}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('ninth', 'Votre service nécessite-t-il de collecter les données de géolocalisation de vos utilisateurs ?', ['class' => 'control-label']); !!}</strong>
                                <br>
                                <div class="align-input">
                                    {!! Form::radio('ninth', '1', false, ['id' => 'nintha', 'required' => '']); !!}
                                    {!! Form::label('nintha', 'Oui, à des fins de fourniture du service, de croisement, et de mise à disposition à des partenaires sous forme anonymisée.') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::radio('ninth', '2', false, ['id' => 'ninthb', 'required' => '']); !!}
                                    {!! Form::label('ninthb', 'Non') !!}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="puce-center">
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('tenth', 'Comptez-vous collecter les données techniques du terminal de l\'utilisateur ? (adresse IP, fournisseur d\'accès à Internet, navigateur utilisé...)', ['class' => 'control-label']); !!}</strong>
                                <br>
                                <div class="align-input">
                                    {!! Form::radio('tenth', '1', false, ['id' => 'tentha', 'required' => '']); !!}
                                    {!! Form::label('tentha', 'Oui') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::radio('tenth', '2', false, ['id' => 'tenthb', 'required' => '']); !!}
                                    {!! Form::label('tenthb', 'Non') !!}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="puce-center">
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('eleventh', 'Comptez-vous avoir recours à des cookies ?', ['class' => 'control-label']); !!}</strong>
                                <br>
                                <div class="align-input">
                                    {!! Form::radio('eleventh', '1', false, ['id' => 'eleventha', 'required' => '']); !!}
                                    {!! Form::label('eleventha', 'Nous n\'utilisons pas de cookies.') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::radio('eleventh', '2', false, ['id' => 'eleventhb', 'required' => '']); !!}
                                    {!! Form::label('eleventhb', 'Nous utilisons des cookies. Leur désactivation ne dégrade pas le fonctionnement des services.') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::radio('eleventh', '3', false, ['id' => 'eleventhc', 'required' => '']); !!}
                                    {!! Form::label('eleventhc', 'Nous utilisons des cookies. Leur désactivation dégrade le fonctionnement des services.') !!}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="puce-center">
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('twelfth', 'Souhaitez-vous conserver les données personnelles de vos utilisateurs ?', ['class' => 'control-label']); !!}</strong>
                                <br>
                                <div class="align-input">
                                    {!! Form::radio('twelfth', '1', false, ['id' => 'twelftha', 'required' => '']); !!}
                                    {!! Form::label('twelftha', 'Non') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::radio('twelfth', '2', false, ['id' => 'twelfthb', 'required' => '']); !!}
                                    {!! Form::label('twelfthb', 'Oui. Vous vous engagez à conserver les données personnelles uniquement pendant la durée de réalisation de la finalité du traitement, et à les supprimer selon les règles en vigueur.') !!}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="puce-center">
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('thirteenth', 'Souhaitez-vous transférer les données personnelles de vos utilisateurs en dehors du territoire de l\'Union Européenne ?', ['class' => 'control-label']); !!}</strong>
                                <p>Plus d'informations : <a href="https://www.cnil.fr/fr/la-protection-des-donnees-dans-le-monde">CNIL - Le niveau de protection des données dans le monde</a></p>
                                <div class="align-input">
                                    {!! Form::radio('thirteenth', '1', false, ['id' => 'thirteentha', 'required' => '']); !!}
                                    {!! Form::label('thirteentha', 'Vous ne souhaitez pas transférer de données personnelles hors de l\'Union Européenne') !!}
                                    <br>
                                </div>
                                <div class="align-input">
                                    {!! Form::radio('thirteenth', '2', false, ['id' => 'thirteenthb', 'required' => '']); !!}
                                    {!! Form::label('thirteenthb', 'Vous souhaitez transférer des données personnelles hors de l\'Union Européenne, dans des pays ayant un niveau de protection suffisant, ou dans d\'autres pays avec l\'autorisation de la CNIL.') !!}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="puce-center">
                        <div class="row">
                            <div class="col-xs-12">
                                <strong>{!! Form::label('fourteenth', 'En cas de litige concernant les présentes CGU, souhaitez-vous avoir recours à l\'arbitrage ? Dans le cas inverse, tout litige sera soumis à l\'application de la législation française.', ['class' => 'control-label']); !!}</strong>
                                <br>
                                <div class="align-input">
                                    {!! Form::radio('fourteenth', '1', false, ['id' => 'fourteentha', 'required' => '']); !!}
                                    {!! Form::label('fourteentha', 'Oui, je souhaite avoir recours à l\'arbitrage') !!}
                                    <br>
                                </div>
                            </div>
                            <div class="align-input">
                                {!! Form::radio('fourteenth', '2', false, ['id' => 'fourteenthb', 'required' => '']); !!}
                                {!! Form::label('fourteenthb', 'Non, je ne souhaite pas avoir recours à l\'arbitrage') !!}
                            </div>
                        </div>
                    </li>
            </div>
        </div>
        </ol>
        {!! Form::submit('Générer la politique de confidentialité', ['class' => 'btn btn-danger generate']); !!}
        {!! Form::close() !!}
    </div>
    </div>
@endsection

