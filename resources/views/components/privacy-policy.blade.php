@extends('layouts.app')

@include('partials.topbar')

@section('content')
    <section class="politique-de-confidentialite">
        <xmp>
            <h1 style="text-align: center;">Politique de confidentialité</h1><br>
            <h3 style="text-align: center;">Sécurité et protection des données personnelles</h3>
            <br>
            <ul style="list-style-type:none; padding-left: 0;">
                <li>
                    <h3>Nature des données collectées</h3>
                    <p><strong>Dans le cadre de l'utilisation des Sites, l'Éditeur est susceptible de collecter les catégories de données suivantes concernant ses Utilisateurs :</strong></p>
                    <ul>
                        @forelse($firsts as $first)
                            <li><p>{{$first}}</p></li>
                        @empty
                        @endforelse
                    </ul>
                </li>
                <br>
                <li>
                    <h3>Communication des données personnelles à des tiers</h3>
                    @if($layouts['second'] == '1')
                        <p><strong>Communication aux autorités sur la base des obligations légales</strong><br>Sur la base des obligations légales, vos données personnelles pourront être divulguées en application d'une loi, d'un règlement ou en vertu d'une décision d'une autorité réglementaire ou judiciaire compétente. De manière générale, nous nous engageons à nous conformer à toutes les règles légales qui pourraient empêcher, limiter ou réglementer la diffusion d’informations ou de données et notamment à se conformer à la Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.</p>
                    @endif
                    @if($layouts['second'] == '2')
                        <p><strong>Pas de communication à des tiers</strong><br>Vos données ne font l'objet d'aucune communication à des tiers. Vous êtes toutefois informés qu'elles pourront être divulguées en application d'une loi, d'un règlement ou en vertu d'une décision d'une autorité réglementaire ou judiciaire compétente.</p>
                    @endif
                    @if(isset($thirds))
                        @forelse($thirds as $third)
                            @if(isset($third) && $third == '1')
                                <p><strong>Communication à des tiers en fonction des paramètres du compte</strong><br>Vos données personnelles sont strictement confidentielles et ne peuvent être divulguées à des tiers, sauf en cas d’accord exprès obtenu via les paramètres de votre compte.</p>
                            @endif
                            @if(isset($third) && $third == '2')
                                <p><strong>Communication à des tiers pour sollicitation commerciale pour des produits et services équivalents</strong><br>Si vous avez effectué un achat sur notre Site, nous pouvons, avec nos partenaires commerciaux et de manière occasionnelle, vous tenir informé de nos nouveaux produits, actualités et offres spéciales, par courriel, par courrier postal et par téléphone quant à des produits ou services similaires aux produits ou services qui ont fait l'objet de votre commande.</p>
                            @endif
                            @if(isset($third) && $third == '3')
                                <p><strong>Communication à des tiers sous forme agrégée et anonymisée</strong><br>Vos données personnelles pourront être utilisées pour enrichir nos bases de données. Elles pourront être transmises à des tiers après avoir été anonymisées et exclusivement à des fins statistiques.</p>
                            @endif
                            @if(isset($third) && $third == '4')
                                <p><strong>Communication à des tiers partenaires</strong><br>Nous pouvons mettre certaines données personnelles à la disposition de partenaires stratégiques travaillant avec nous, pour la fourniture de produits et services ou nous aidant à commercialiser nos produits auprès des clients.</p>
                            @endif
                            @if(isset($third) && $third == '5')
                                <p><strong>Communication à des tiers avec restrictions liées aux destinataires</strong><br>Les données personnelles que vous nous communiquez lors de votre commande sont transmises à nos fournisseurs et filiales pour le traitement de celle-ci. Ces informations sont considérées par nos fournisseurs et filiales comme étant strictement confidentielles.</p>
                            @endif
                            @if(isset($third) && $third == '6')
                                <p><strong>Communication à des tiers pour marketing direct</strong><br>Nous pouvons partager des informations avec des partenaires publicitaires afin de vous envoyer des communications promotionnelles, ou pour vous montrer plus de contenu sur mesure, y compris de la publicité pertinente pour les produits et services susceptibles de vous intéresser, et pour comprendre comment les utilisateurs interagissent avec les publicités.</p>
                            @endif
                        @empty
                        @endforelse
                    @endif
                </li>
                <br>
                @if(isset($layouts['fourth']))
                    <li>
                        <h3>Viralité des conditions de réutilisation des données personnelles</h3>
                        @if(isset($layouts['fourth']) && $layouts['fourth'] == '1')
                            <p><strong>Engagement sur la viralité des conditions</strong><br>En cas de communication de vos données personnelles à un tiers, ce dernier est tenu d'appliquer des conditions de confidentialité identiques à celle du Site.</p>
                        @endif
                        @if(isset($layouts['fourth']) && $layouts['fourth'] == '2')
                            <p><strong>Pas d’engagement sur la viralité des conditions et soumission à des obligations contractuelles spécifiques</strong><br>En cas de communication de vos données personnelles à un tiers, l’Éditeur se réserve le droit de fixer contractuellement avec vous les modalités de réutilisation des données.</p>
                        @endif
                        @if(isset($layouts['fourth']) && $layouts['fourth'] == '3')
                            <p><strong>Pas d'engagement sur la viralité des conditions de réutilisation</strong><br>En cas de communication de vos données personnelles à un tiers, les conditions de confidentialité du tiers s'appliquent.</p>
                        @endif
                    </li>
                    <br>
                @endif
                <li>
                    <h3>Information préalable pour la communication des données personnelles à des tiers en cas de fusion / absorption</h3>
                    @if($layouts['fifth'] == '1')
                        <p><strong>Collecte de l’opt-in (consentement) préalable à la transmission des données suite à une fusion / acquisition</strong><br>Dans le cas où nous prendrions part à une opération de fusion, d’acquisition ou à toute autre forme de cession d’actifs, nous nous engageons à obtenir votre consentement préalable à la transmission de vos données personnelles et à maintenir le niveau de confidentialité de vos données personnelles auquel vous avez consenti.</p>
                    @endif
                    @if($layouts['fifth'] == '2')
                        <p><strong>Information préalable et possibilité d’opt-out avant et après la fusion / acquisition</strong><br>Dans le cas où nous prendrions part à une opération de fusion, d’acquisition ou à toute autre forme de cession d’actifs, nous nous engageons à garantir la confidentialité de vos données personnelles et à vous informer avant que celles-ci ne soient transférées ou soumises à de nouvelles règles de confidentialité.</p>
                    @endif
                </li>
                <br>
                <li>
                    <h3>Finalité de la réutilisation des données personnelles collectées</h3>
                    @forelse($sixths as $sixth)
                        @if(isset($sixth) && $sixth == '1')
                            <p><strong>Effectuer les opérations relatives à la gestion des clients concernant :</strong><br>
                            <ul>
                                <li>les contrats ; les commandes ; les livraisons ; les factures ; la comptabilité et en particulier la gestion des comptes clients</li>
                                <li>un programme de fidélité au sein d'une entité ou plusieurs entités juridiques ;</li>
                                <li>le suivi de la relation client tel que la réalisation d'enquêtes de satisfaction, la gestion des réclamations et du service après-vente</li>
                                <li>la sélection de clients pour réaliser des études, sondages et tests produits (sauf consentement des personnes concernées recueilli dans les conditions prévues à l’article 6, ces opérations ne doivent pas conduire à l'établissement de profils susceptibles de faire apparaître des données sensibles - origines raciales ou ethniques, opinions philosophiques, politiques, syndicales, religieuses, vie sexuelle ou santé des personnes)</li>
                            </ul>
                            </p>
                        @endif
                        @if(isset($sixth) && $sixth == '2')
                            <p><strong>Effectuer des opérations relatives à la prospection :</strong><br>
                            <ul>
                                <li>la gestion d'opérations techniques de prospection (ce qui inclut notamment les opérations techniques comme la normalisation, l'enrichissement et la déduplication)</li>
                                <li>la sélection de personnes pour réaliser des actions de fidélisation, de prospection, de sondage, de test produit et de promotion. Sauf consentement des personnes concernées recueilli dans les conditions prévues par ce texte, ces opérations ne doivent pas conduire à l'établissement de profils susceptibles de faire apparaître des données sensibles (origines raciales ou ethniques, opinions philosophiques, politiques, syndicales, religieuses, vie sexuelle ou santé des personnes)
                                </li>
                                <li>la réalisation d'opérations de sollicitations</li>
                            </ul>
                            </p>
                        @endif
                        @if(isset($sixth) && $sixth == '3')
                            <p><strong>L'élaboration de statistiques commerciales</strong></p>
                        @endif
                        @if(isset($sixth) && $sixth == '4')
                            <p><strong>La cession, la location ou l'échange de ses fichiers de clients et de ses fichiers de prospects</strong></p>
                        @endif
                        @if(isset($sixth) && $sixth == '5')
                            <p><strong>L’actualisation de ses fichiers de prospection par l’organisme en charge de la gestion de la liste d’opposition au démarchage téléphonique, en application des dispositions du code de la consommation</strong></p>
                        @endif
                        @if(isset($sixth) && $sixth == '6')
                            <p><strong>L'organisation de jeux concours, de loteries ou de toute opération promotionnelle à l'exclusion des jeux d'argent et de hasard en ligne soumis à l'agrément de l'Autorité de Régulation des Jeux en Ligne</strong></p>
                        @endif
                        @if(isset($sixth) && $sixth == '7')
                            <p><strong>La gestion des demandes de droit d'accès, de rectification et d'opposition</strong></p>
                        @endif
                        @if(isset($sixth) && $sixth == '8')
                            <p><strong>La gestion des impayés et du contentieux, à condition qu'elle ne porte pas sur des infractions et / ou qu'elle n'entraîne pas une exclusion de la personne du bénéfice d'un droit, d'une prestation ou d'un contrat</strong></p>
                        @endif
                        @if(isset($sixth) && $sixth == '9')
                            <p><strong>La gestion des avis des personnes sur des produits, services ou contenus</strong></p>
                        @endif
                    @empty
                    @endforelse
                </li>
                <br>
                <li>
                    <h3>Agrégation des données</h3>
                    <p><strong>Agrégation avec des données non personnelles</strong><br>Nous pouvons publier, divulguer et utiliser les informations agrégées (informations relatives à tous nos Utilisateurs ou à des groupes ou catégories spécifiques d'Utilisateurs que nous combinons de manière à ce qu'un Utilisateur individuel ne puisse plus être identifié ou mentionné) et les informations non personnelles à des fins d'analyse du secteur et du marché, de profilage démographique, à des fins promotionnelles et publicitaires et à d'autres fins commerciales.</p>
                    <p><strong>Agrégation avec des données personnelles disponibles sur les comptes sociaux de l'Utilisateur</strong><br>Si vous connectez votre compte à un compte d’un autre service afin de faire des envois croisés, ledit service pourra nous communiquer vos informations de profil, de connexion, ainsi que toute autre information dont vous avez autorisé la divulgation. Nous pouvons agréger les informations relatives à tous nos autres Utilisateurs, groupes, comptes, aux données personnelles disponibles sur l’Utilisateur.</p>
                </li>
                <br>
                <li>
                    <h3>Collecte des données d'identité</h3>
                    @if($layouts['seventh'] == '1')
                        <p><strong>Consultation libre</strong><br>La consultation du Site ne nécessite pas d'inscription ni d'identification préalable. Elle peut s'effectuer sans que vous ne communiquiez de données nominatives vous concernant (nom, prénom, adresse, etc). Nous ne procédons à aucun enregistrement de données nominatives pour la simple consultation du Site.</p>
                    @endif
                    @if($layouts['seventh'] == '2')
                        <p><strong>Utilisation d’un pseudonyme</strong><br>L’utilisation du Site nécessite une inscription sans identification préalable. Elle peut s’effectuer sans que vous ne communiquiez de données nominatives vous concernant (nom, prénom, adresse, etc). Vous pouvez utiliser le pseudonyme de votre choix.</p>
                    @endif
                    @if($layouts['seventh'] == '3')
                        <p><strong>Inscription et identification préalable pour la fourniture du service</strong><br>L’utilisation du Site nécessite une inscription et une identification préalable. Vos données nominatives (nom, prénom, adresse postale, e-mail, numéro de téléphone,...) sont utilisées pour exécuter nos obligations légales résultant de la livraison des produits et / ou des services, en vertu du Contrat de licence utilisateur final, de la Limite de garantie, le cas échéant, ou de toute autre condition applicable. Vous ne fournirez pas de fausses informations nominatives et ne créerez pas de compte pour une autre personne sans son autorisation. Vos coordonnées devront toujours être exactes et à jour.</p>
                    @endif
                </li>
                <br>
                <li>
                    <h3>Collecte des données d'identification</h3>
                    @if($layouts['eighth'] == '1')
                        <p><strong>Utilisation de l'identifiant de l'utilisateur pour proposition de mise en relation et offres commerciales</strong><br>Nous utilisons vos identifiants électroniques pour rechercher des relations présentes par connexion, par adresse mail ou par services. Nous pouvons utiliser vos informations de contact pour permettre à d'autres personnes de trouver votre compte, notamment via des services tiers et des applications clientes. Vous pouvez télécharger votre carnet d'adresses afin que nous soyons en mesure de vous aider à trouver des connaissances sur notre réseau ou pour permettre à d'autres Utilisateurs de notre réseau de vous trouver. Nous pouvons vous proposer des suggestions, à vous et à d'autres Utilisateurs du réseau, à partir des contacts importés de votre carnet d’adresses. Nous sommes susceptibles de travailler en partenariat avec des sociétés qui proposent des offres incitatives. Pour prendre en charge ce type de promotion et d'offre incitative, nous sommes susceptibles de partager votre identifiant électronique.</p>
                    @endif
                    @if($layouts['eighth'] == '2')
                        <p><strong>Utilisation de l'identifiant de l’utilisateur uniquement pour l’accès aux services</strong><br>Nous utilisons vos identifiants électroniques seulement pour et pendant l'exécution du contrat.</p>
                    @endif
                </li>
                <br>
                @if(isset($layouts['eighth']) && $layouts['eighth'] == '1')
                    <li>
                        <h3>Géolocalisation</h3>
                        @if($layouts['eighth'] == '1')
                            <p><strong>Géolocalisation à des fins de fourniture du service</strong><br>Nous collectons et traitons vos données de géolocalisation afin de vous fournir nos services. Nous pouvons être amenés à faire usage des données personnelles dans le but de déterminer votre position géographique en temps réel. Conformément à votre droit d'opposition prévu par la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, vous avez la possibilité, à tout moment, de désactiver les fonctions relatives à la géolocalisation.<br><strong>Géolocalisation à des fins de croisement</strong><br>Nous collectons et traitons vos données de géolocalisation afin de permettre à nos services d'identifier les points de croisement dans le temps et dans l'espace avec d'autres Utilisateurs du service afin de vous présenter le profil des Utilisateurs croisés. Conformément à votre droit d'opposition prévu par la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, vous avez la possibilité, à tout moment, de désactiver les fonctions relatives à la géolocalisation. Vous reconnaissez alors que le service ne sera plus en mesure de vous présenter de profil des autres Utilisateurs.<br><strong>Géolocalisation avec mise à disposition des partenaires pour référencement et agrégation (avec opt-in)</strong><br>Nous pouvons collecter et traiter vos données de géolocalisation avec nos partenaires. Nous nous engageons à anonymiser les données utilisées. Conformément à votre droit d'opposition prévu par la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, vous avez la possibilité, à tout moment, de désactiver les fonctions relatives à la géolocalisation.</p>
                        @endif
                        @if($layouts['eighth'] == '2')
                            <p>Nous ne collectons pas vos de données de géolocalisation.</p>
                        @endif
                    </li>
                    <br>
                @endif
                <li>
                    <h3>Collecte des données du terminal</h3>
                    @if($layouts['tenth'] == '1')
                        <p><strong>Collecte des données de profilage et des données techniques à des fins de fourniture du service</strong><br>Certaines des données techniques de votre appareil sont collectées automatiquement par le Site. Ces informations incluent notamment votre adresse IP, fournisseur d'accès à Internet, configuration matérielle, configuration logicielle, type et langue du navigateur... La collecte de ces données est nécessaire à la fourniture des services.<br><strong>Collecte des données techniques à des fins publicitaires, commerciales et statistiques</strong><br>Les données techniques de votre appareil sont automatiquement collectées et enregistrées par le Site, à des fins publicitaires, commerciales et statistiques. Ces informations nous aident à personnaliser et à améliorer continuellement votre expérience sur notre Site. Nous ne collectons ni ne conservons aucune donnée nominative (nom, prénom, adresse...) éventuellement attachée à une donnée technique. Les données collectées sont susceptibles d’être revendues à des tiers.</p>
                    @endif
                    @if($layouts['tenth'] == '2')
                        <p><strong>Aucune collecte des données techniques</strong><br>Nous ne collectons et ne conservons aucune donnée technique de votre appareil (adresse IP, fournisseur d'accès à Internet...).</p>
                    @endif
                </li>
                <br>
                <li>
                    <h3>Cookies</h3>
                    <p><strong>Durée de conservation des cookies</strong><br>Conformément aux recommandations de la CNIL, la durée maximale de conservation des cookies est de 13 mois au maximum après leur premier dépôt dans le terminal de l'Utilisateur, tout comme la durée de la validité du consentement de l’Utilisateur à l’utilisation de ces cookies. La durée de vie des cookies n’est pas prolongée à chaque visite. Le consentement de l’Utilisateur devra donc être renouvelé à l'issue de ce délai.</p>
                    <p><strong>Finalité cookies</strong><br>Les cookies peuvent être utilisés pour des fins statistiques notamment pour optimiser les services rendus à l'Utilisateur, à partir du traitement des informations concernant la fréquence d'accès, la personnalisation des pages ainsi que les opérations réalisées et les informations consultées. Vous êtes informé que l'Éditeur est susceptible de déposer des cookies sur votre terminal. Le cookie enregistre des informations relatives à la navigation sur le service (les pages que vous avez consultées, la date et l'heure de la consultation...) que nous pourrons lire lors de vos visites ultérieures.</p>
                    @if($layouts['eleventh'] == '1')
                        <p><strong>Opt-in pour le dépôt de cookies</strong><br>Nous n'utilisons pas de cookies. Si nous devions en utiliser à l’avenir, vous en seriez informé préalablement et auriez la possibilité de désactiver ces cookies.</p>
                    @endif
                    @if($layouts['eleventh'] == '2')
                        <p><strong>Droit de l'Utilisateur de refuser les cookies</strong><br>Vous reconnaissez avoir été informé que l'Éditeur peut avoir recours à des cookies. Si vous ne souhaitez pas que des cookies soient utilisés sur votre terminal, la plupart des navigateurs vous permettent de désactiver les cookies en passant par les options de réglage.</p>
                    @endif
                    @if($layouts['eleventh'] == '3')
                        <p><strong>Droit de l'Utilisateur de refuser les cookies, la désactivation entraînant un fonctionnement dégradé du service</strong><br>Vous reconnaissez avoir été informé que l'Éditeur peut avoir recours à des cookies, et l'y autorisez. Si vous ne souhaitez pas que des cookies soient utilisés sur votre terminal, la plupart des navigateurs vous permettent de désactiver les cookies en passant par les options de réglage. Toutefois, vous êtes informé que certains services sont susceptibles de ne plus fonctionner correctement.<br><strong>Association possible des cookies avec des données personnelles pour permettre le fonctionnement du service</strong><br>L'Éditeur peut être amenée à recueillir des informations de navigation via l'utilisation de cookies.</p>
                    @endif
                </li>
                <br>
                <li>
                    <h3>Conservation des données techniques</h3>
                    <p><strong>Durée de conservation des données techniques</strong><br>Les données techniques sont conservées pour la durée strictement nécessaire à la réalisation des finalités visées ci-avant.
                    </p>
                </li>
                <br>
                <li>
                    <h3>Délai de conservation des données personnelles et d'anonymisation</h3>
                    @if($layouts['twelfth'] == '1')
                        <p><strong>Pas de conservation des données</strong><br>Nous ne conservons aucune donnée personnelle au delà de votre durée de connexion au service pour les finalités décrites dans les présentes CGU.</p>
                    @endif
                    @if($layouts['twelfth'] == '2')
                        <p><strong>Conservation des données pendant la durée de la relation contractuelle</strong><br>Conformément à l'article 6-5° de la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, les données à caractère personnel faisant l'objet d'un traitement ne sont pas conservées au-delà du temps nécessaire à l'exécution des obligations définies lors de la conclusion du contrat ou de la durée prédéfinie de la relation contractuelle.<br><strong>Conservation des données anonymisées au delà de la relation contractuelle / après la suppression du compte</strong><br>Nous conservons les données personnelles pour la durée strictement nécessaire à la réalisation des finalités décrites dans les présentes CGU. Au-delà de cette durée, elles seront anonymisées et conservées à des fins exclusivement statistiques et ne donneront lieu à aucune exploitation, de quelque nature que ce soit.</p>
                    @endif
                    <p><strong>Suppression des données après suppression du compte </strong><br>Des moyens de purge de données sont mis en place afin d'en prévoir la suppression effective dès lors que la durée de conservation ou d'archivage nécessaire à l'accomplissement des finalités déterminées ou imposées est atteinte. Conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, vous disposez par ailleurs d'un droit de suppression sur vos données que vous pouvez exercer à tout moment en prenant contact avec l'Éditeur.</p>
                    <p><strong>Suppression des données après 3 ans d'inactivité </strong><br>Pour des raisons de sécurité, si vous ne vous êtes pas authentifié sur le Site pendant une période de trois ans, vous recevrez un e-mail vous invitant à vous connecter dans les plus brefs délais, sans quoi vos données seront supprimées de nos bases de données.</p>
                </li>
                <br>
                <li>
                    <h3>Suppression du compte</h3>
                    <p><strong>Suppression du compte à la demande</strong><br>L'Utilisateur a la possibilité de supprimer son Compte à tout moment, par simple demande à l'Éditeur OU par le menu de suppression de Compte présent dans les paramètres du Compte le cas échéant.
                    </p>
                    <p><strong>Suppression du compte en cas de violation des CGU</strong><br>En cas de violation d'une ou de plusieurs dispositions des CGU ou de tout autre document incorporé aux présentes par référence, l'Éditeur se réserve le droit de mettre fin ou restreindre sans aucun avertissement préalable et à sa seule discrétion, votre usage et accès aux services, à votre compte et à tous les Sites.</p>
                </li>
                <br>
                <li>
                    <h3>Indications en cas de faille de sécurité décelée par l'Éditeur</h3>
                    <p><strong>Information de l'Utilisateur en cas de faille de sécurité</strong><br>Nous nous engageons à mettre en oeuvre toutes les mesures techniques et organisationnelles appropriées afin de garantir un niveau de sécurité adapté au regard des risques d'accès accidentels, non autorisés ou illégaux, de divulgation, d'altération, de perte ou encore de destruction des données personnelles vous concernant. Dans l'éventualité où nous prendrions connaissance d'un accès illégal aux données personnelles vous concernant stockées sur nos serveurs ou ceux de nos prestataires, ou d'un accès non autorisé ayant pour conséquence la réalisation des risques identifiés ci-dessus, nous nous engageons à :</p>
                    <ul>
                        <li><p>Vous notifier l'incident dans les plus brefs délais ;</p></li>
                        <li><p>Examiner les causes de l'incident et vous en informer ;</p></li>
                        <li><p>Prendre les mesures nécessaires dans la limite du raisonnable afin d'amoindrir les effets négatifs et préjudices pouvant résulter dudit incident</p></li>
                    </ul>
                    <br>
                    <p><strong>Limitation de la responsabilité </strong><br>En aucun cas les engagements définis au point ci-dessus relatifs à la notification en cas de faille de sécurité ne peuvent être assimilés à une quelconque reconnaissance de faute ou de responsabilité quant à la survenance de l'incident en question.</p>
                </li>
                <br>
                <li>
                    <h3>Transfert des données personnelles à l'étranger</h3>
                    @if($layouts['thirteenth'] == '1')
                        <p><strong>Pas de transfert en dehors de l'Union européenne</strong><br>L'Éditeur s'engage à ne pas transférer les données personnelles de ses Utilisateurs en dehors de l'Union européenne.
                        </p>
                    @endif
                    @if($layouts['thirteenth'] == '2')
                        <p><strong>Transfert des données dans des pays avec un niveau de protection équivalent</strong><br>L'Éditeur s'engage à respecter la réglementation applicable relative aux transferts des données vers des pays étrangers et notamment selon les modalités suivantes :
                        <ul>
                            <li>L'Éditeur transfère les données personnelles de ses Utilisateurs vers des pays reconnus comme offrant un niveau de protection équivalent.
                            </li>
                            <li>L'Éditeur transfère les données personnelles de ses Utilisateurs en dehors des pays reconnus par la CNIL comme ayant un niveau de protection suffisant : L'Éditeur a obtenu une autorisation de la CNIL pour procéder à ce transfert.
                            </li>
                        </ul><br>Pour connaître la liste de ces pays : <a href="https://www.cnil.fr/fr/la-protection-des-donnees-dans-le-monde">CNIL - protection des données dans le monde</a></p>
                    @endif
                </li>
                <br>
                <li><h3>Modification des CGU et de la politique de confidentialité</h3>
                    <p><strong>En cas de modification des présentes CGU, engagement de ne pas baisser le niveau de confidentialité de manière substantielle sans l'information préalable des personnes concernées</strong><br>Nous nous engageons à vous informer en cas de modification substantielle des présentes CGU, et à ne pas baisser le niveau de confidentialité de vos données de manière substantielle sans vous en informer et obtenir votre consentement.</p></li>
                <br>
                <li>
                    <h3>Droit applicable et modalités de recours</h3>
                    @if($layouts['fourteenth'] == '1')
                        <p><strong>Clause d'arbitrage</strong><br>Vous acceptez expressément que tout litige susceptible de naître du fait des présentes CGU, notamment de son interprétation ou de son exécution, relèvera d'une procédure d'arbitrage soumise au règlement de la plateforme d'arbitrage choisie d'un commun accord, auquel vous adhérerez sans réserve.</p>
                    @endif
                    @if($layouts['fourteenth'] == '2')
                        <p><strong>Application du droit français (législation CNIL) et compétence des tribunaux</strong><br>Les présentes CGU et votre utilisation du Site sont régies et interprétées conformément aux lois de France, et notamment à la Loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. Le choix de la loi applicable ne porte pas atteinte à vos droits en tant que consommateur conformément à la loi applicable de votre lieu de résidence. Si vous êtes un consommateur, vous et nous acceptons de se soumettre à la compétence non-exclusive des juridictions françaises, ce qui signifie que vous pouvez engager une action relative aux présentes CGU en France ou dans le pays de l'UE dans lequel vous vivez. Si vous êtes un professionnel, toutes les actions à notre encontre doivent être engagées devant une juridiction en France.<br>En cas de litige, les parties chercheront une solution amiable avant toute action judiciaire. En cas d'échec de ces tentatives, toutes contestations à la validité, l'interprétation et / ou l'exécution des présentes CGU devront être portées même en cas de pluralité des défendeurs ou d'appel en garantie, devant les tribunaux français.</p>
                    @endif
                </li>
                <br>
                <li>
                    <h3>Portabilité des données</h3>
                    <p><strong>Portabilité des données</strong><br>L'Éditeur s'engage à vous offrir la possibilité de vous faire restituer l'ensemble des données vous concernant sur simple demande. L'Utilisateur se voit ainsi garantir une meilleure maîtrise de ses données, et garde la possibilité de les réutiliser. Ces données devront être fournies dans un format ouvert et aisément réutilisable.
                    </p>
                </li>
                <br>
            </ul>
        </xmp>
    </section>
@endsection
