@extends('layouts.app')

@include('partials.topbar')

@section('content')
    <div class="row" id="legal-notice">
        <h2>Formulaire de création de mentions légales</h2>
        <div class="col-md-12">
            {!! Form::open(['method' => 'POST', 'url' => '/mentions-legales']) !!}
            <div class="form-container">
                <h2>Propriétaire du site web</h2>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('url', 'Adresse URL de votre site internet', ['class' => 'control-label']); !!}
                        {!! Form::text('url', '',['placeholder' => 'https://www.yoursite.com'], ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('forme_juridique', 'Forme juridique', ['class' => 'control-label']); !!}
                        {!! Form::text('forme_juridique', '',['placeholder' => 'SARL, SAS, ...'], ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('client_name', 'Nom de l\'entreprise', ['class' => 'control-label']); !!}
                        {!! Form::text('client_name', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('client_mail', 'Email de contact', ['class' => 'control-label']); !!}
                        {!! Form::text('client_mail', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('client_boss', 'Nom du représentant légal de
                                        l’entreprise', ['class' => 'control-label']); !!}
                        {!! Form::text('client_boss', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('client_address', 'Adresse du siège social de l\'entreprise', ['class' => 'control-label']); !!}
                        {!! Form::text('client_address', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('client_zip_code', 'Code postal', ['class' => 'control-label']); !!}
                        {!! Form::text('client_zip_code', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('client_city', 'Ville', ['class' => 'control-label']); !!}
                        {!! Form::text('client_city', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('client_capital_social', 'Capital social', ['class' => 'control-label']); !!}
                        {!! Form::text('client_capital_social', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('client_num_tva', 'Numéro de TVA
                                        Intracommunautaire', ['class' => 'control-label']); !!}
                        {!! Form::text('client_num_tva', '', ['required']); !!}
                    </div>
                </div>
            </div>
            <div class="flex">
            <div class="form-container">
                <h2>Concepteur du site web<br><small>Requis si différent du réalisateur</small></h2>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_forme_juridique', 'Forme juridique', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_forme_juridique', '',['placeholder' => 'SARL, SAS, ...']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_name', 'Nom de l\'entreprise', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_name', ''); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_address', 'Adresse du siège social de l\'entreprise', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_address', ''); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_zip_code', 'Code postal', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_zip_code', ''); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_city', 'Ville', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_city', ''); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_capital_social', 'Capital social', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_capital_social', ''); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_rcs', 'Numéro d\'immatriculation RCS', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_rcs', ''); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_mail', 'Email', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_mail', ''); !!}
                    </div>
                </div>
            </div>
            <div class="form-container realisateur">
                <h2>Réalisateur du site web</h2>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_forme_juridique', 'Forme juridique', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_forme_juridique', $yeswedev?'SAS':'', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_name', 'Nom de l\'entreprise', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_name', $yeswedev?'Yes We Dev':'', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_address', 'Adresse du siège social de l\'entreprise', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_address', $yeswedev?'8 quai Robinot de Saint Cyr':'', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_zip_code', 'Code postal', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_zip_code', $yeswedev?'35000':'', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_city', 'Ville', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_city', $yeswedev?'Rennes':'', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_capital_social', 'Capital social', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_capital_social',$yeswedev?'20 000 €':'', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_rcs', 'Numéro d\'immatriculation RCS', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_rcs', $yeswedev?'801 172 776 02 30 96 43 32':'', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('concept_mail', 'Email', ['class' => 'control-label']); !!}
                        {!! Form::text('concept_mail', $yeswedev?'kenavo@yeswedev.fr':'', ['required']); !!}
                    </div>
                </div>
            </div>
            </div>
            <div class="form-container">
                <h2>Responsable de la publication
                    <small>(Personne physique ou morale)</small>
                </h2>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('resp_publication_name', 'Nom', ['class' => 'control-label']); !!}
                        {!! Form::text('resp_publication_name', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('resp_publication_mail', 'Contact email ou téléphone', ['class' => 'control-label']); !!}
                        {!! Form::text('resp_publication_mail', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('client_court_city', 'Ville du tribunal de compétence
                                        en cas
                                        de litige', ['class' => 'control-label']); !!}
                        {!! Form::text('client_court_city', '', ['required']); !!}
                    </div>
                </div>
            </div>
            <div class="form-container">
                <h2>Hébergement</h2>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('hebergeur_name', 'Nom de l\'hébergeur', ['class' => 'control-label']); !!}
                        {!! Form::text('hebergeur_name', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('hebergeur_address', 'Adresse de l\'hébergeur', ['class' => 'control-label']); !!}
                        {!! Form::text('hebergeur_address', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('hebergeur_zip_code', 'Code postal', ['class' => 'control-label']); !!}
                        {!! Form::text('hebergeur_zip_code', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('hebergeur_city', 'Ville', ['class' => 'control-label']); !!}
                        {!! Form::text('hebergeur_city', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('hebergeur_phone', 'Numéro de téléphone', ['class' => 'control-label']); !!}
                        {!! Form::text('hebergeur_phone', '', ['required']); !!}
                    </div>
                </div>
            </div>
            <div class="form-container">
                <h2>Délégué à la protection des données
                    <small>(Personne physique ou morale)</small>
                </h2>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('dpo_name', 'Nom', ['class' => 'control-label']); !!}
                        {!! Form::text('dpo_name', '', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('dpo_mail', 'Contact email', ['class' => 'control-label']); !!}
                        {!! Form::text('dpo_mail', '', ['required']); !!}
                    </div>
                </div>
            </div>
            <div class="form-container">
                <h2>Données collectées </h2>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('collect_data_1', 'Données indispensables au fonctionnement
                                du
                                service', ['class' => 'control-label']); !!}
                        {!! Form::text('collect_data_1', $yeswedev?'nom, prénom, adresse postale, email, N° de téléphone':'', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('data_store_time_1', 'Durée de conservation de ces données
                                (en
                                mois)', ['class' => 'control-label']); !!}
                        {!! Form::text('data_store_time_1', '',['placeholder' => '12'], ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('collect_data_2', 'Données permettant d’améliorer
                                l’expérience
                                utilisateur', ['class' => 'control-label']); !!}
                        {!! Form::text('collect_data_2', $yeswedev?'Le type de matériel utilisé, le navigateur utilisé , l’adresse IP, et les différents événements survenus lors de votre visite sur le site.':'', ['required']); !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('data_store_time_2', 'Durée de conservation de ces données
                                (en
                                mois)', ['class' => 'control-label']); !!}
                        {!! Form::text('data_store_time_2', '',['placeholder' => '12'], ['required']); !!}
                    </div>
                </div>
            </div>
            <div class="form-container btn-generate">
            {!! Form::submit('Générer les mentions légales', ['class' => 'btn btn-danger generate']); !!}
            {!! Form::close() !!}
            </div>
        </div>
    </div>



@endsection
