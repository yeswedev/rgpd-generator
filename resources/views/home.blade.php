@extends('layouts.app')

@section('content')
<div class="home">
    <img class="logo-home" src="/img/logo_ywd-rgpd.svg" alt="">
    <a href="/formulaire-mentions-legales" class="legal-btn"><i class="fa fa-plus"></i> Mentions légales</a>
    <a href="/formulaire-politique-de-confidentialite" class="legal-btn"><i class="fa fa-plus"></i> Politique de confidentialité</a>
    <p class="message">
        <span><i class="fa fa-exclamation-triangle"></i></span>
        <span>Aucune</span>
        <span>donnée</span>
        <span>n'</span>
        <span>est</span>
        <span>stockée</span>
        <span><i class="fa fa-exclamation-triangle"></i></span>
    </p>
</div>



@endsection
