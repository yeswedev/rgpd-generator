<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials.head')
</head>


<body class="">

<div id="wrapper">

    {{--<div class="responsive-message">--}}
        {{--<p>RGPD-Generator n’est pas encore adapté pour les mobiles et tablettes, nous y travaillons. Utilise plutôt ton ordinateur pour un affichage optimal.</p>--}}
    {{--</div>--}}

    @yield('content')

</div>


@include('partials.javascripts')
</body>
</html>