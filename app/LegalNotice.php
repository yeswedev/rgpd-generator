<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;

class LegalNotice extends Model
{
    use FormAccessible;
    protected $fillable = ['url', 'forme_juridique', 'client_name', 'client_address', 'client_zip_code', 'client_city', 'client_capital_social', 'client_num_tva', 'concept_forme_juridique', 'concept_name', 'concept_address', 'concept_zip_code', 'concept_city', 'concept_capital_social', 'concept_rcs', 'resp_publication_name', 'resp_publication_mail', 'client_boss', 'client_court_city', 'hebergeur_name', 'hebergeur_address', 'hebergeur_zip_code', 'hebergeur_city', 'hebergeur_phone', 'dpo_name', 'dpo_mail', 'collect_data_1', 'data_store_time_1', 'collect_data_2', 'data_store_time_2',  ];


}