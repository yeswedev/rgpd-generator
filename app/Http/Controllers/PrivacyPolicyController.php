<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\FormPrivacyPolicy;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use Auth;

class PrivacyPolicyController extends Controller
{
    public function index()
    {
        return view('form-privacy-policy');
    }


    public function show(FormPrivacyPolicy $request)
    {
        $layouts = $request->all();

        $firsts = $request->input('first');
        $thirds = $request->input('third');
        $sixths = $request->input('sixth');

        return view('components.privacy-policy', compact('layouts', 'firsts', 'thirds', 'sixths'));

    }


}