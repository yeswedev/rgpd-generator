<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Project;
use App\User;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{

    public function index()
    {

        return view('home');

    }


}
