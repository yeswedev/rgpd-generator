<?php

namespace App\Http\Controllers;

use App\Project;
use App\User;
use Illuminate\Http\Request;
use Auth;


class LegalNoticeController extends Controller
{

    public function show(Request $request)
    {
        $layout = $request->all();
        return view('components.legal-notice', compact('layout'));

    }

    public function index(Request $request)
    {
        $yeswedev = false;
        if($request->query('yeswedev') && $request->query('yeswedev') == 1){
            $yeswedev = true;
        }
        return view('form-legal-notice', array('yeswedev' => $yeswedev));

    }


}